from urllib.request import urlopen
from bs4 import BeautifulSoup
import operator
import re
import string


def cleanInput(inputs):
    inputs = re.sub('\n+', " ", inputs)
    inputs = re.sub('\[[0-9]*\]', '', inputs)
    inputs = re.sub(' +', ' ', inputs)
    inputs = bytes(inputs, 'UTF-8')
    inputs = inputs.decode('ascii', 'ignore')
    cleanInput = []
    inputs = inputs.split(' ')
    for item in inputs:
        item = item.strip(string.punctuation)
        if len(item) > 1 or (item.lower() == 'a' or item.lower() == 'i'):
            cleanInput.append(item)
    return cleanInput


def ngrams(inputs, n):
    inputs = cleanInput(inputs)
    outputs = {}
    for i in range(len(inputs) - n + 1):
        ngramTemp = ' '.join(inputs[i: i+n])
        if ngramTemp not in outputs:
            outputs[ngramTemp] = 0
        outputs[ngramTemp] += 1
    return outputs


# html = urlopen("http://en.wikipedia.org/wiki/Python_(programing_language)")
content = str(urlopen("http://pythonscraping.com/files/inaugurationSpeech.txt").read(), 'utf-8')
# bsObj = BeautifulSoup(html)
# content = bsObj.find('div', {'id': 'mw-content-text'}).get_text()
ngrams = ngrams(content, 2)
sortedNgrams = sorted(ngrams.items(), key=operator.itemgetter(1), reverse=True)
print(sortedNgrams)
# print(ngrams)
# print("counts:" + len(ngrams))
