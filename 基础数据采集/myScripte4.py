from urllib.request import urlopen
from bs4 import BeautifulSoup


html = urlopen("http://www.pythonscraping.com/pages/page3.html")
bsObj = BeautifulSoup(html)

for child in bsObj.find('table', {'id': 'giftList'}).children:
    print(child)
print("####################################################")
# 处理兄弟标签
for sibling in bsObj.find('table', {'id': 'giftList'}).tr.next_siblings:
    print(sibling)

print("######################################################")
# 处理父标签
print(bsObj.find('img', {'src': '../img/gifts/img1.jpg'}
                 ).parent.previous_sibling.get_text())
