import os
from urllib.request import urlopen
from urllib.request import urlretrieve
from bs4 import BeautifulSoup

'''
html = urlopen("https://www.pythonscraping.com")
bsObj = BeautifulSoup(html)
imageLocation = bsObj.find('a', {'id': 'logo'}).find('img')['src']
urlretrieve(imageLocation, 'logo.jpg')
'''

downloadDirectory = "dowloaded"
baseUrl = "https://pythonscraping.com"


def getAbsoluteURL(baseUrl, source):
    if(source.startwith("https://www.")):
        url = "https://" + source[11:]
    elif source.startwith("https://"):
        url = source
    elif source.startwith("www."):
        url = "https://" + source[4:]
    else:
        url = baseUrl + "/" + source

    if baseUrl not in url:
        return None
    return url


def getDownloadPath(baseUrl, absoluteUrl, downloadDirectory):
    path = absoluteUrl.replace("www.", "")
    path = path.replace(baseUrl, "")
    path = downloadDirectory + path
    directory = os.path.dirname(path)

    if not os.path.exists(directory):
        os.mkdir(directory)

    return path


html = urlopen("https://www.pythonscraping.com")
bsObj = BeautifulSoup(html)
dowloadList = bsObj.findAll(src=True)

for dowload in dowloadList:
    fileUrl = getAbsoluteURL(baseUrl, dowload["src"])
    if fileUrl is not None:
        print(fileUrl)
        urlretrieve(fileUrl, getDownloadPath(
            baseUrl, fileUrl, downloadDirectory))
